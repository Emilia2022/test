package com.emiliy.ratelimiter;

import io.github.bucket4j.Bandwidth;
import io.github.bucket4j.Bucket;
import io.github.bucket4j.Bucket4j;
import io.github.bucket4j.Refill;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.awt.*;
import java.awt.geom.Area;
import java.time.Duration;

@RestController
public class Controller {
    private final Bucket bucket;

    public Controller() {
        Bandwidth limit = Bandwidth.classic(10, Refill.greedy(10, Duration.ofMinutes(1)));
        this.bucket = Bucket4j.builder()
                .addLimit(limit)
                .build();
    }
    @GetMapping(value = "/api")
    public ResponseEntity<String> checkIn(  ) {
        if(bucket.getAvailableTokens()==0){
            System.out.println(bucket.getAvailableTokens()+"avalible tokens");
            return ResponseEntity.status(HttpStatus.TOO_MANY_REQUESTS).build();
        }
return ResponseEntity.ok("okkey");

    }

}
